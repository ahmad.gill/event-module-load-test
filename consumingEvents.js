const { events } = require('.');
const fs = require('fs');
const path = require('path');
const { getConsumer } = require("@adnoc-dist/adnoc-event-module");

const eventModule = {
    KAFKA_CLIENT_LOGLEVEL: 2,
    NAME: 'development1',
    BROKERS_LIST: "b-2.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-1.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-3.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098",
    TOPIC_PARITION_COUNT : 2
}


const setUpEventListeners  = async ()=>{
    const consumer = getConsumer({
        group_id: "test-service-1"
    }, eventModule);
    
    const UserUpdatedListener = require("./listeners/userUpdatedListener")
    const EquipmentInfoChanged1 = require("./listeners/eqipmentInfoChanged1.js")
    const EquipmentInfoChanged2 = require("./listeners/eqipmentInfoChanged2.js")
    const OrderSettledListener1 = require("./listeners/orderSettledListener1")
    const OrderSettledListener2 = require("./listeners/orderSettledListener2")
    await consumer.listenToEvent(new EquipmentInfoChanged1());
    await consumer.listenToEvent(new EquipmentInfoChanged2());
    await consumer.listenToEvent(new OrderSettledListener1());
    await consumer.listenToEvent(new OrderSettledListener2());
     await consumer.listenToEvent(new UserUpdatedListener());
    
    await consumer.run();
}

setUpEventListeners().then(()=>{
    console.log("Event Listeners Set up properly");
})
