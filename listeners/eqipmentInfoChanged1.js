const { AbstractListener } = require('@adnoc-dist/adnoc-event-module/listener');
const { getConsumer, getProducer, commands, events } = require("@adnoc-dist/adnoc-event-module");
class EquipmentInfoChanged1 extends AbstractListener {
    constructor(options) {
        super(events.washAndGo.EquipmentInfoChanged, {
            retry : true,
            haveFailureFallBack : true,
            delayInterval : 1200,
            maxRetries : 2
        })
        this.count = 0
    }

    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    async run(event) {
        console.log("EVENT --- EquipmentInfoChanged1-listner sleeping", event)
        await this.sleep(10000)
        console.log("EVENT --- EquipmentInfoChanged1", event)
        // const min = 1;
        // const max = 100;
        // const randomInRange = Math.floor(Math.random() * (max - min + 1)) + min;
        // if (randomInRange % 2 == 0) {
        //     throw new Error("Something went wrong")
        // } 
    }

    fallBackRun(event) {
        console.log("FallbackRun - EVENT2 ---", event)
    }

    getIdentifier(){
        return this.constructor.name;
    }
}

module.exports = EquipmentInfoChanged1