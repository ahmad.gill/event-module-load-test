const express = require('express');
const app = express();
const { getConsumer, getProducer, commands, events } = require("@adnoc-dist/adnoc-event-module");
const eventModule = {
    KAFKA_CLIENT_LOGLEVEL: 2,
    NAME: 'development1',
    TOPIC_PARITION_COUNT : 2,
    TOPIC_REPLICATION_FACTOR : 2,
    BROKERS_LIST: "b-2.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-1.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098,b-3.devmsk2.3i59ki.c8.kafka.eu-west-1.amazonaws.com:9098"
}
const producer = getProducer(eventModule);
require('./consumingEvents');
const count = 0;
// Endpoint handler for GET /hello
app.get('/hello', (req, res) => {
    const min = 1;
    const max = 100;
    const randomInRange = Math.floor(Math.random() * (max - min + 1)) + min;
    
    let event = null;
    if (randomInRange % 2 == 0) {
        event = new events.user.UserUpdated({ a : 1 , b : 2 , c : 3}, 123 , {cacheId : 898})
    } else {
        event = new events.washAndGo.EquipmentInfoChanged({ x : 23, y : 45 ,z : 67})
    }
    producer.produceEvent(event);
    res.send('Hello, world!');

});

app.get('/hello1', (req, res) => {
    const min = 1;
    const max = 100;
    const randomInRange = Math.floor(Math.random() * (max - min + 1)) + min;
    
    let event = null;
    if (randomInRange % 2 == 0) {
        event = new events.user.UserUpdated({ a : 1 , b : 2 , c : 3}, 123, {cacheId : 23})
    } else {
        event = new events.order.OrderSettled({ a : 1 , b : 2 , c : 3}, 123, {cacheId : 23});
    }
    producer.produceEvent(event);
    res.send('Hello, world!');

});
// Start the server
const port = 3003;
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
    const min = 1;
    const max = 100;
    const randomInRange = Math.floor(Math.random() * (max - min + 1)) + min;
    console.log(randomInRange)
});
